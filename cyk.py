import csv
	
class node:
	def __init__(self, value=None):
		self.value = value
		self.children = []
	def add_child(self,child):
		self.children.append(child)

def cyk(tokens,grammar):
	num_tokens = len(tokens)

	# we need to be able to refer to production LHS by their respective RHS
	index_by_rhs = {}
	for prod in grammar.productions():
		index_by_rhs[prod.rhs()] = prod.lhs()

	# default table entries
	table = [[[] for i in range(num_tokens+1)] for j in range(num_tokens+1)]
	
	# initialize diagonal elements
	for i in range(num_tokens):
		unit_prods = grammar.productions(rhs=tokens[i])
		if len(unit_prods) > 0:
			n = node([unit_prods[0].lhs()])
			n.add_child([node([unit_prods[0].rhs()])])
			table[i][i+1].append(n)
	# diagonals initialized

	for span in range(2,num_tokens+1):
		for start_span in range(num_tokens+1-span):
			end_span = start_span + span
			for partition in range(start_span+1,end_span):
				non_term1_nodes, non_term2_nodes = table[start_span][partition], table[partition][end_span]
				for n1nodes in non_term1_nodes:
					for n2nodes in non_term2_nodes:
						for n1 in n1nodes.value:
							for n2 in n2nodes.value:
								if (n1,n2) in index_by_rhs:
									n = node([index_by_rhs[(n1,n2)]])
									n.add_child([n1nodes,n2nodes])
									table[start_span][end_span].append(n)
	return table

def display(table,lentokens):
	for i in range(lentokens+1):
		for j in range(lentokens+1):
			elements = table[i][j]
			if len(elements)==0:
				print '.','      ',
			else:
				print elements[0].value,
				for e in elements[1:]:
					print ',',e.value,
				print '      ',
		print '\n'


def print_table(table,lentokens):
	writer = csv.writer(open('table.csv','w+'))
	for i in range(lentokens+1):
		headerrow = []
		for j in range(lentokens+1):
			elements = table[i][j]
			if len(elements)==0:
				headerrow.append('.')
			else:
				cell = []
				for e in elements:
					cell.append(e.value)
				headerrow.append(cell)
		writer.writerow(headerrow)
			
def print_tree(root,depth=0):
	if depth == 0:
		print '\n\n'

	for i in range(depth):
		print '\t',
	print root.value
	for child in root.children:
		for element in child:
			print_tree(element,depth+1)
