
verb_tags = ['VB','VBD','VBG','VBN']
corpus = open('brown.txt').read().strip()

verbrules = open('verbs.cfg','w+')

word_tags = corpus.split(' ')

for wt in word_tags:
	word = wt.split('_')[0]
	tag = wt.split('_')[1]

	if tag in verb_tags:
		content = tag + " -> " + "\"" + word + "\"" + '\n'
		verbrules.write(content)
