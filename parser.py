from cyk import cyk,display,print_tree,print_table
import pickle

### GRAMMAR DEFINITION ###

grammar = pickle.load(open('grammar.pkl'))
### GRAMMAR DEFINITION ###

while True:
	sentence = raw_input("Enter Verb Group\n")
	tokens = sentence.strip().split()
	### The tokenized input sentence. This is just here for testing; get input sentence from user and then tokenize into list as below
	#tokens = ["will","have","gone"]

	### Call the CYK implementation to generate the parse table
	table=cyk(tokens,grammar)

	### Display the table
	#display(table,len(tokens))
	print_table(table,len(tokens))

	root_value = [grammar.start()]

	for root in table[0][len(tokens)]:
		print_tree(root)
