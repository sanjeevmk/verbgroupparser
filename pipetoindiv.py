
grammar = open('grammar1.cfg').read().strip()
fh = open('goodgrammar1.cfg','w+')

productions = grammar.split('\n')

for p in productions:
	lhs = p.strip().split("->")[0]
	rhses = p.strip().split("->")[1].strip().split("|")
	for rhs in rhses:
		content = lhs + "->" + rhs + "\n"
		fh.write(content)
